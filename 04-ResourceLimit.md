# Resource Limits
- Setting resource limits to prevent software from misbehaving
- Containers can help ensure that software only uses computing resources and access the data that we expect.
- We can provide 
  - resource allowances
  - access shared memory 
  - run programs as specific users 
  - control the type of change the container can do on the host

## Setting Resource limits
- System resources such as memory and CPU time are scarce
- Exceeding the system resources can lead to degraded performance of the process and may stop
- We can limit the resources like memory, CPU
- Default allowance is limited by available resource


## Memory limits
- Basic restriction that can be placed on the container
- To ensure the container does not allocate all of system's memory and starve other processes
- Place the limit using the ``-m`` or ``--memory`` flag during the ``run`` command
- They are not reservations and only protection from overconsumption
- Allowed formats are ``b``, `k`, `m` and `g`
- If the process exhausts the memory, docker does not detect the problem nor attempts to mitigate. It can best apply the restart logic specified using the ``--restart`` flag 

```
docker container run -d --name ch6_mariadb \
    --memory 256m \
    --cpu-shares 1024 \
    --cap-drop net_raw \
    -e MYSQL_ROOT_PASSWORD=test \
    mariadb:5.5
```

## CPU limits
- Effect of starvation is performance degradation instead of failure 
- A slow process is worst than the failing one 
- Can specify relative weight for the container relative to other containers
- Percentage is the sum of all computing cycles of all the processors available to the container
- Use the `--cpu-shares` during `docker run` or `docker create` command
  
```
docker container run -d -P --name wordpress-container \
--memory 512m \
--cpu-shares 512 \
--link ch6_mariadb:mysql \
-e WORDPRESS_DB_PASSWORD=test \
wordpress:5.0.0-php7.2-apache
```

## Access to devices
- Unlike Memory and CPU resource restriction, this is more of resource authorization control.
- Use the `--device` flag to specify a set of devices to mount into the container

```
docker container run -it --rm \
    --device /dev/video0:/dev/video0 \
    ubuntu:16.04 ls -al /dev
```

## Shared Memory
- Each container has its own shared memory namespace 
- Programs running inside different containers cannot access the shared namespaces 
- Joining the containers namespace is done using `--ipc` flag
- Reusing shared memory across containers has security implications but is a safer alternative than sharing memory with the host (using the `--ipc=host`)


## Default User
- Docker starts the container as the user specified in the image metadata by default
- Default is the `root` user
- Root user has full previliged access to the state of the container
- Processes running as that user inherit those permissions
- Bug in the process can damage the container and to prevent these issues is to not use the `root` user
- Exceptions to run as root user is to run system administrative activities inside the container

## Working with User
- Default user is specified in the image
- To check run the `docker inspect` command on specific container or image
- Blank username indicate the `root` user
- The `user` can be changed by the `entrypoint` or `cmd` during startup
- The metadata run by the `inspect` command will not reflect the changes and will include the user defined in the configuration
  
    




