## What is Docker ?
- Not a programming language
- Not a framework
- Open-source project to build, ship and run programs 
- Uses OS technology called **containers**
- Makes deployment declarative, repeatable and trustworthy
- Promotes disposable paradigm, persistent state isolation 

## Use cases of Docker
- Higher production change confidence
- Faster iteration
- Building expressive CI pipelines 
- Decrease member onboarding time in local dev environment
- Eliminate incosistencies 
- Lets developers work on dependencies, installation and packaging 
- Lets system administrators inject env specific configuration and control access to system resources.
- Lets system administrators focus on higher value activities

## Hello world with Docker

Run the first docker container
```
docker run classpathio/hello_world
```

- The container is running until the program is running
- The docker image will be downloaded only once and will be cached locally

## Introducing Containers 
- Any software running inside a docker is running inside a container
- Containers are not Virtualization 
- Containers are new unit of deployment
- Virtual machines provide virtual hardware 
  - They take long time to start
  - Provide hardware abstractions to run OS
  - Consume significant resources 
  - They run the entire OS in addition to running software 
  - Can perform optimally once they are up but
  - Start up delays make them a poor fit for just in time or reactive deployment scenario
- Unlike VMs
  - Docker containers dont use hardware  Virtualization technology
  - Programs running inside the container interface directly with the host's kernel
  - Leverages the container technology built into the OS kernel

## Why Docker 
- Before software installation
  - Consider OS requirements 
  - Resources that the software requires 
  - Dependencies of the software 
- Package Managers (``apt``, ``yum``) solves the dependency management 
- Multiple applications can be running on the same OS
- When two applications sharing the common dependency might not work well where there are two versions of the same dependency
- Push from the software community to adopt containers 
- Defacto to work in current era of building microservices, devops and cloud-native applications

### Shipping Containers 
- Like a physical shipping container 
- Box with application and all its dependencies 
- Imporves Portability
- Secures application since the process run inside jails
- Like apps on the Cell phone
- Provides abstraction
- Removing software is also easier
- No lingering artifacts after the container is removed


## Environment agnostic systems 
- Read only file systems 
- Environmental variable injection 
- Volumes

  
  