# Dockerfile
- A txt file with set of instructions executed from top to bottom
- Dockerfile is the preferrred way to build docker images
  - Predictable
  - Repeatable
  - Versioned
- First instructions should always be `FROM` or `SCRATCH`
- After executing every instruction a new container will be created
- Comments are included using `#` sign
- `Dockerfile` is the default name, else use the `-f` or `--file` flag to build with the user defined file name
- Common commands
    - `FROM`
    - `LABEL`
    - `RUN` executes the program with the arguments (Installing softwares)
    - `ENTRYPOINT`


## Docker instructions
 - Categorized into Meta instructions and Filesystem instructions

## Meta Instructions
- Files that should not be copied into the image should be specified in `.dockerignore` file
- `ENV` sets the environment variables. similar to `--env` flag on `docker container run` command
- `WORKDIR` - The working directory. Will be created inside the container if does not exists
- `EXPOSE` - Creates a image layer and opens the TCP port
- `ENTRYPOINT` instruction sets the executable 
  - both `SHELL` form and `EXEC` form

## File system Instructions
- `COPY` - to copy the files from the host to the container
  - Takes at least two arguments
  - Last argument in the destination
  - All others are the source files and will be copied with file ownership set to root
  - COPY can be used with both SHELL and EXEC forms
- `VOLUME`
  - Similar to `--volume` during `docker container run` command
  - No way to specify `bind-mount` or `read-only` mount

- `CMD`
  - Closely related to `ENTRYPOINT` command
  - Takes either the SHELL or EXEC form

- `ADD`
  - Similar to `COPY` command
  - Fetches remote files if a URL is specified
  - Can extract the files of any source determined to be an archive file

  
