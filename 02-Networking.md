## Networking
- Networking is about communicating between processes that many or may not share the same lcoal resources.
- Deeper understang isn't required to use the tools provided by Docker
- Protocol, Interface (address and represents location) and Ports
- Bridge is created by docker to connect multiple contatainers to act as a single network
- Docker abstracts the underlying host-attached network from the containers
- Containers attached to Docker will get a IP address that is routable from other containers attached to the same Docker network
- Docker containers cannot determine the IP address of the Host
- Docker containers cannot host the service outside of the current network
- Just like Volumes, `Network` are first class resource and have their own independent life cycle using the `docker network` commands

```
docker network ls
```
### Bridge network
- Good for single server use
- Its a virtual network
- Not cluster aware 
- Container IP address are not discoverable and routable outside of the host machine
- Local to the machine where Docker is installed 
- Creates routes between the containers where the host is attached
- Containers have `loopback` interface and virtual ethernet interface linked to another virtual interface on the host's namespace 
- Network connections outside the container uses Network Address Translation NAT as means to connect and making it look like the connection from host itself.
  

### Creating a Bridge network 
```
docker network create \
    --driver bridge \
    --label example=bride \
    --attachable \
    --scope local \
    --subnet 10.0.42.0/24 \
    --ip-range 10.0.42.128/25 \
  network-one
```

```
docker network list
NETWORK ID          NAME                DRIVER              SCOPE
6435fd136fae        bridge              bridge              local
d32534f60e9e        host                host                local
831364bd59ed        network-one         bridge              local
fd254af0189b        none                null                local
```

- ``attachable`` allows to attach and dettach containers to this network anytime

```
docker network create \
    --driver bridge \
    --label example=bride \
    --attachable \
    --scope local \
    --subnet 10.0.43.0/24 \
    --ip-range 10.0.43.128/25 \
  network-two
```

### Limitations of Bridge network
- Useful for single server deployments 
- Cannot route between containers on different host machines 
- Cannot handle machine failure 

## Drivers for Cluster level networking
### Underlay networks

- Underlay networks - macvlan and ipvlan
- Underlay networks creates first class network for each container 
- Each container running inside a host looks like an independent node on the network 
- Underlay network depend on the host network and hence not portable 

### Overlay networks
- Popular multihost container networking option
- Available for Docker engines when swarm mode is enabled 
- Similar to creating bridge network but are multi-host aware 
- Can router intercontainer connections between nodes in the swarm
- Containers are not routable from outside the cluster 
- Network definitions are independent of host network environment 

## Host
- Not network and are network attachment type with a special meaning
- When specifying ``--network host`` during ``docker run`` command, the container is started without any special network adaptors or network namespace. 
- Software running inside the container will have the same degree of access to the host network as it would be running outside the container 
- There is no network namespace
- All kernel tools for tuning the network stack are available for modification (as long as the modifying process has access to do so )
- Useful for system level services and infrastructural components
- Not appropriate for multi-tenant environment and should be dissallowed for third-party containers 

Example:
```
docker run --rm \
    --network host \
    alpine:3.8 ip -o addr
```


## None
- Instructs Docker not to provision any virtual Ethernet adaptors to the created container.
- Uses its own namespace and will be isolated. 
- Without adaptors connected across the namespace boundary, will not be able to communicate outside the container.
- Containers will still have a loop-back interface.
- Multi processes running inside the container can still access using localhost for interprocess communication.
- The process running inside the container cannot reach outside the container
- Nothing outside the container can connect to the interface 

```
 docker run --rm --network none alpine:3.8 ip -o addr
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1000\    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
1: lo    inet 127.0.0.1/8 scope host lo\       valid_lft forever preferred_lft forever
```

### Use cases
1. Applications which need network isolation
2. Programs to generate random password


## NodePort publishing
- Connecting the containers with external network requires extra step
- Specifically tell Docker how to forward traffic from external network interfaces using NodePort
- Achieved using `-p` or ``publish``
- Forward all TCP port 8080 from all host interfaces to TCP port in the container.

  ```
    0.0.0.0:8080/tcp

    8080:8080/tcp

    8080:8080
  ```
  Random host port and 8080 on the container
  ```
    docker run --rm \
    -p 8080 \
    alpine:3.8 echo "forward ephemeral TCP -> container TCP 8080"
  ```
  Exposing multiple ports
  ```
  docker run --rm \
  -p 127.0.0.1:8080:8080/tcp \
  -p 127.0.0.1:3000:3000/tcp \
  alpine:3.8 echo "forward multiple TCP ports from localhost"
  ```

  ```
  docker run -d \
  -p 8080:8080 \
  -p 3000 \
  -p 7500:76 \
  --name multi-listener \
  alpine:3.8 sleep 300

  docker port multi-listener 3000
  docker port multi-listener 8080
  docker port multi-listener 76
  ```

  ## Caveats 
  - No firewalls and Network policies between containers
  - Containers on the same container network will have mutual (bidirectional) unrestricted network access


## DNS
- Protocol for mapping hostnames to IP addresses
- Decoupling hostname from IP address
- Containers on the bridge network have private IP addresses and aren't publicaly accessible

Option 1:
- Docker ``--name`` to set the hostname during ``docker run`` command
- Other containers do not know its hostname, use cases are limited
  
```
docker run --rm \
    --hostname mickymouse \
    alpine:3.8 \
    nslookup mickymouse
```

Option 2: Specify the DNS server
- The value must be an IP address
-  
```
docker run --rm \
    --dns 8.8.8.8 \
    alpine:3.8 \
    nslookup docker.com
```

## Externalized Network Management
- to provide direct management of container network configuration, discovery and other network related resources.
- In this case, set the Docker to ``none`` network.
- Docker is still responsible for creating network namespace for the container 
- Docker in this case will not create or manage network interfaces
- Will not be able to use docker tooling to inspect the network configuration and port mapping.
- 

Example: Kubernetes