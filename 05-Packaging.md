# Packaging Softwares in Docker images

## Packaging Software Applications
- Uses *Union File Systems (UFS)*
- Any changes done to the filesystem inside a container will be written as new layers owned by the container that created them.
- When committing a new image, the new layer of files will be added to the image.
- The following parameters will be forwarded to the image from the container
  - All the Environmental variables
  - Working directory
  - Set of exposed ports
  - Volume definitions 
  - Container Entrypoint
  - Commands and Arguments
  
## Union File Systems
- Made up of layers
- Uses **copy-on-write**, where the file to be written is first copied from the *read-only* layer into the *writable-layer* before the changes is made.
- When reading the files from the UFS, the files will be read from the top most layer
- Both addition and deletion works on the top most layer 
- Deleting the files will hide the files from the bottom layers
- Use the `docker container diff` to view the changes 
- Use `docker container commit` command to commit the layer. A unique Id is generated
- A docker image is the stack of layers which are constructed by traversing the layer dependency graph till the starting layer
- Layers are traversed from top to bottom
- Every commit will have a unique commit id from which we can create containers
- To make it human friendly, tags can be associated with the commit id
- By default the latest tag will be associated with the top most layer 
- `docker build` and `docker tag` commands are used to create images and tag
- All layers below the writable layers are immutable 
- We can create additional images using branching technique.
  
## Versioning considerations
- Consider consistent versionsing like semantic ``major.minor.patch``
- `latest` always should point to the next unversioned release candidate 
- Ensure that `latest` is always stable as this has become the convention among the community and is used frequently than it should be. 