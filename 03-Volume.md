## Volumes
- Linux unifies all storage into a single tree .
- All storage devices like USB, disk are attached to the specific location on the tree.
- The locations are called mount points
- Mount point defines 
  - location in the tree
  - access properties to the data at that point (read, read-write)
  - source of data (disks,USB, hard-disk, virtual disk)

- The image that the container is created from is mounted at the container's file tree root (/)
- Every container has different set of mount points 
- This is how containers can access storage from host systems and can share data between containers
- Three types of storage mount points 
  - Bind mounts
  - In memory storage
  - Docker volumes

## Bind Mounts
- Mount points used to remount parts of a file system tree to other locations.
- Specifically, rebind the user defined location on the host file system to a specific location on the container's file tree
- Container can access the data from the host system as well as dump the output/logs to the storage location on the host system

### Dissadvantages
- Cannot port the containers and make them generic as the mountpoints are configured with host system details
- Multiple containers bound by the same mount point on the host can compete for file lock

## In-Memory storage
- Special type of mount
- To store sensitive information  
- By default no space restriction, but can be added by 

```
  docker run --rm \ 
   --mount type=tmpfs,dst=/tmp\
   --entrypoint mount \
   alpine:latest -v 
```

```
docker run --rm \
    --mount type=tmpfs,dst=/tmp,tmpfs-size=16k,tmpfs-mode=1770 \
    --entrypoint mount \
    alpine:latest -v
```

## Docker volumes
- Named file system managed by Docker
- Used to decouple storage from specialized locations
- All operations can be accomplished with `docker volume` subcommand set
- Docker uses default `local` plugin to create volume on the host
- The volume will be controlled by docker engine
- Similar to software vs database
- Is used as a **Polymorphic tool**/**Polymorphic container pattern**
- Anonymous volumes will be deleted when the container is deleted 
- Named volumes should be explicitly deleted using the `docker volume rm` command
- Volume currently is use cannot be deleted
- To delete the volumes which are not in use use the `docker volume prune` command 

```
docker volume create \
    --driver local \
    --label example=location \
    location-example
docker volume inspect \
    --format "{{json .Mountpoint}}" \
    location-example
```

```
docker volume create \
    --driver local \
    --label example=cassandra \
    cass-shared
```

```
docker run -d \
    --volume cass-shared:/var/lib/cassandra/data \
    --name cass1 \
    cassandra:2.2
```
- Delete docker volume 

```
docker volume rm cass-shared
```